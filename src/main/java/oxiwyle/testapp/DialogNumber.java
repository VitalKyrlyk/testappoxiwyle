package oxiwyle.testapp;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DialogNumber extends AppCompatDialogFragment{

    @BindView(R.id.tvNumber)
    TextView tvNumber;

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));

        LayoutInflater inflater = getActivity().getLayoutInflater();
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.dialog_fragment, null);
        ButterKnife.bind(this, view);

        builder.setView(view)
                .setTitle("Нажата кнопка номер: ")
                .setPositiveButton("OK", (dialogInterface, i) -> {

                });

        assert getArguments() != null;
        int number = getArguments().getInt("number");
        tvNumber.setText(Integer.toString(number));

        return builder.create();
    }
}
