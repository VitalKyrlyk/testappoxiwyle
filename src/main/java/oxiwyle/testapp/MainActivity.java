package oxiwyle.testapp;

import android.app.DialogFragment;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.btnAccelerated)
    ImageButton btnAccelerated;

    @BindView(R.id.btnPause)
    ImageButton btnPause;

    @BindView(R.id.btnNormal)
    ImageButton btnNormal;

    @BindView(R.id.btnShop)
    ImageButton btnShop;

    @BindView(R.id.btnMenu)
    ImageButton btnMenu;

    @OnClick(R.id.btnMenu)
    public void btnMenu(){
        if (btnMenu.isSelected()){
            btnMenu.setSelected(false);
            btnMenu.setBackgroundResource(R.drawable.tb_ic_menu);
        } else {
            btnMenu.setSelected(true);
            btnMenu.setBackgroundResource(R.drawable.tb_ic_menu_clicked);
        }
        startDialogFragment(1);
    }

    @OnClick(R.id.btnShop)
    public void btnShop(){
        if (btnShop.isSelected()){
            btnShop.setSelected(false);
            btnShop.setBackgroundResource(R.drawable.tb_ic_shop);
        } else {
            btnShop.setSelected(true);
            btnShop.setBackgroundResource(R.drawable.tb_ic_shop_clicked);
        }
        startDialogFragment(2);
    }

    @OnClick(R.id.btnNormal)
    public void btnNormal(){
        if (btnNormal.isSelected()){
            btnNormal.setSelected(false);
            btnNormal.setBackgroundResource(R.drawable.tb_ic_normal);
        } else {
            btnNormal.setSelected(true);
            btnNormal.setBackgroundResource(R.drawable.tb_ic_normal_clicked);
        }
        startDialogFragment(4);
    }

    @OnClick(R.id.btnPause)
    public void btnPause(){
        if (btnPause.isSelected()){
            btnPause.setSelected(false);
            btnPause.setBackgroundResource(R.drawable.tb_ic_pause);
        } else {
            btnPause.setSelected(true);
            btnPause.setBackgroundResource(R.drawable.tb_ic_pause_clicked);
        }
        startDialogFragment(3);
    }

    @OnClick(R.id.btnAccelerated)
    public void btnAccelerated(){
        if (btnAccelerated.isSelected()){
            btnAccelerated.setSelected(false);
            btnAccelerated.setBackgroundResource(R.drawable.tb_ic_accelerated);
        } else {
            btnAccelerated.setSelected(true);
            btnAccelerated.setBackgroundResource(R.drawable.tb_ic_accelerated_clicked);
        }
        startDialogFragment(5);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if (savedInstanceState != null){
            btnMenu.setSelected(savedInstanceState.getBoolean("btnMenu"));
            btnNormal.setSelected(savedInstanceState.getBoolean("btnNormal"));
            btnAccelerated.setSelected(savedInstanceState.getBoolean("btnAccelerated"));
            btnPause.setSelected(savedInstanceState.getBoolean("btnPause"));
            btnShop.setSelected(savedInstanceState.getBoolean("btnShop"));

            if (btnMenu.isSelected()){
                btnMenu.setBackgroundResource(R.drawable.tb_ic_menu_clicked);
            } else {
                btnMenu.setBackgroundResource(R.drawable.tb_ic_menu);
            }

            if (btnNormal.isSelected()){
                btnNormal.setBackgroundResource(R.drawable.tb_ic_normal_clicked);
            } else {
                btnNormal.setBackgroundResource(R.drawable.tb_ic_normal);
            }

            if (btnAccelerated.isSelected()){
                btnAccelerated.setBackgroundResource(R.drawable.tb_ic_accelerated_clicked);
            } else {
                btnAccelerated.setBackgroundResource(R.drawable.tb_ic_accelerated);
            }

            if (btnPause.isSelected()){
                btnPause.setBackgroundResource(R.drawable.tb_ic_pause_clicked);
            } else {
                btnPause.setBackgroundResource(R.drawable.tb_ic_pause);
            }

            if (btnShop.isSelected()){
                btnShop.setBackgroundResource(R.drawable.tb_ic_shop_clicked);
            } else {
                btnShop.setBackgroundResource(R.drawable.tb_ic_shop);
            }

        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("btnMenu", btnMenu.isSelected());
        outState.putBoolean("btnShop", btnShop.isSelected());
        outState.putBoolean("btnPause", btnPause.isSelected());
        outState.putBoolean("btnAccelerated", btnAccelerated.isSelected());
        outState.putBoolean("btnNormal", btnNormal.isSelected());
    }

    public void startDialogFragment(int number){
        DialogNumber dialogNumber = new DialogNumber();
        assert getFragmentManager() != null;
        Bundle arg = new Bundle();
        arg.putInt("number", number);
        dialogNumber.setArguments(arg);
        dialogNumber.show(getSupportFragmentManager(), "Dialog");
    }
}
